import React from "react";
import { useHistory } from "react-router-dom";
import { FaGripfire } from "react-icons/fa";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import validationSchema from "../yupSchema";
import "./Form.css";
//import * as bcrypt from "bcrypt";

function SignUp() {
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm({
        resolver: yupResolver(validationSchema),
    });

    let history = useHistory();

    const Submit = (data) => {
        const credentials = JSON.stringify(data);
        localStorage.setItem("credentials", credentials);

        history.push("/login");
    };

    return (
        <div className="form-container">
            <span className="login-header">
                <FaGripfire />
                Login
            </span>
            <span className="login-account">
                Already Have an Account ?{" "}
                <a href="/login" className="link">
                    Login
                </a>
            </span>
            <form onSubmit={handleSubmit(Submit)}>
                <div className="form-group">
                    <div className="row">
                        <label className="formLabel">Username</label>
                    </div>
                    <input
                        className="form-control"
                        placeholder="John Doe"
                        {...register("username", {
                            required: true,
                            minLength: 6,
                        })}
                        name="username"
                    />

                    <small className="text-danger">
                        {errors.username?.message}
                    </small>
                </div>
                <div className="form-group">
                    <div className="row">
                        <label className="formLabel">Email</label>
                    </div>
                    <input
                        className="form-control"
                        placeholder="name@example.com"
                        {...register("email", {
                            required: true,
                            pattern: /\S+@\S+\.\S+/,
                        })}
                        name="email"
                    />
                    <small className="text-danger">
                        {errors.email?.message}
                    </small>
                </div>
                <div className="form-group">
                    <div className="row">
                        <div className="col">
                            <label className="formLabel">Password</label>
                        </div>
                    </div>
                    <input
                        type="password"
                        className="form-control"
                        placeholder="Enter Your password"
                        {...register("password", {
                            required: true,
                            minLength: 8,
                        })}
                        name="password"
                    />
                    <small className="text-danger">
                        {errors.password?.message}
                    </small>
                </div>
                <div className="form-group">
                    <div className="row">
                        <div className="col">
                            <label className="formLabel">
                                Confirm Password
                            </label>
                        </div>
                    </div>
                    <input
                        type="password"
                        className="form-control"
                        placeholder="Re-enter your password"
                        {...register("confirmPassword", { required: true })}
                        name="confirmPassword"
                    />
                    <small className="text-danger">
                        {errors.confirmPassword?.message}
                    </small>
                </div>
                <button className="btn form-button">Sign Up</button>
            </form>
        </div>
    );
}

export default SignUp;
