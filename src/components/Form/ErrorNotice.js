import React from "react";
import { FaTimesCircle } from "react-icons/fa";

export default function ErrorNotice(props) {
    return (
        <div className="alert alert-dismissible alert-danger">
            <span>{props.message}</span>

            <FaTimesCircle
                onClick={props.clearError}
                style={{ marginLeft: "10px" }}
            />
        </div>
    );
}
