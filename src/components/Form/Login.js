import React, { useState } from "react";
import "./Form.css";
import { useHistory } from "react-router-dom";
import { FaGripfire } from "react-icons/fa";
import { FaGithub, FaGoogle } from "react-icons/fa";
import ErrorNotice from "./ErrorNotice";

function Login() {
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [error, setError] = useState();
    let history = useHistory();

    const creds = JSON.parse(localStorage.getItem("credentials"));

    const Email = creds.email;
    const Password = creds.password;

    const submit = async (e) => {
        e.preventDefault();

        if ((password && email) == null) {
            setError("Please enter all Field");
        } else if (email !== Email) {
            setError("User doesn't exists");
        } else if (password !== Password) {
            setError("Wrong Password");
        } else {
            sessionStorage.setItem("isAuthenticated", true);
            history.push("/todo");
        }
    };

    return (
        <div className="form-container">
            <span className="login-header">
                <FaGripfire />
                Login
            </span>
            <span className="login-account">
                Don't have an account ?{" "}
                <a href="/signup" className="link">
                    Sign Up
                </a>
            </span>
            <form onSubmit={submit}>
                <div className="form-group">
                    {error && (
                        <ErrorNotice
                            message={error}
                            clearError={() => setError(undefined)}
                        />
                    )}
                    <div className="row">
                        <label className="formLabel">Email</label>
                    </div>
                    <input
                        className="form-control"
                        placeholder="name@example.com"
                        name="email"
                        onChange={(e) => setEmail(e.target.value)}></input>
                </div>
                <div className="form-group">
                    <div className="row">
                        <div className="col">
                            <label className="formLabel">Password</label>
                        </div>
                        <div className="col-7">
                            <a href="/forgot-password">Forgot password?</a>
                        </div>
                    </div>
                    <input
                        className="form-control"
                        placeholder="Enter Your password"
                        name="password"
                        onChange={(e) => setPassword(e.target.value)}></input>
                </div>
                <button className="btn form-button">Sign In</button>
                <h3 className="divider">OR</h3>
                <button className="btn git-button">
                    <FaGithub className="icon" />
                    Login with Github
                </button>
                <button className="btn google-button">
                    <FaGoogle className="icon" />
                    Login with Google
                </button>
            </form>
        </div>
    );
}

export default Login;
