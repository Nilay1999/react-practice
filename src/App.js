import "./App.css";
import TodoList from "./components/Todo/TodoList";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Navbar, Nav } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import Login from "./components/Form/Login";
import SignUp from "./components/Form/SignUp";

function App() {
    const isLoggedin = sessionStorage.getItem("isAuthenticated");
    console.log(isLoggedin);
    return (
        <Router>
            <Navbar bg="dark" variant="dark">
                <div className="container">
                    <Navbar.Brand href="/todo">Todo App</Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link>
                            <Link
                                style={{ textDecoration: "none" }}
                                to="/login">
                                Login
                            </Link>
                        </Nav.Link>
                        <Nav.Link>
                            <Link
                                style={{ textDecoration: "none" }}
                                to="/signup">
                                Sign Up
                            </Link>
                        </Nav.Link>
                    </Nav>
                </div>
            </Navbar>
            <div>
                <Switch>
                    <Route path="/todo">
                        <TodoList />
                    </Route>
                    <Route path="/login">
                        <Login />
                    </Route>
                    <Route path="/signup">
                        <SignUp />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
